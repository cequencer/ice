classdef Senses < handle
	properties (SetAccess = private)
        targetWord
        wordSenseCentroids
        wordSenseIDs
        multiSense
        multiSenseCutOff
        clusterIDs
    end
    methods
        function this = Senses(p,targetWord, wordSenseCentroids, wordSenseIDs, clusterIDs)
        %Senses Container class for word senses of a given target word.

            this.targetWord = targetWord; 
            this.wordSenseCentroids = wordSenseCentroids;
            this.wordSenseIDs = wordSenseIDs;
            this.multiSense = p.multiSense;
            this.multiSenseCutOff = p.multiSenseCutOff;
            this.clusterIDs = clusterIDs;
            
        end
        function [id, confidence]= classify(this,instanceVector)
            [ dist, I ] = distance( instanceVector, this.wordSenseCentroids );
            if this.multiSense
                id = '';
                for ic=1:length(I)
                    if ic == 1 || dist(ic) > this.multiSenseCutOff
                        id = [id ' ' this.targetWord '%' this.wordSenseIDs{I(ic)} '/' num2str(dist(ic))];
                    end
                end
            else 
                id = this.wordSenseIDs{I(1)};
            end
            confidence = dist(1);
        end
        
        
    end
end


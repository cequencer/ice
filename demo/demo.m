clc;

addpath('../','../lib/')

%create parameter struct
p = struct();

%% define paths
p.textfilename = 'data/text8';
p.wordvectorpath = 'bin/vectors.bin';
p.contextvectorpath ='bin/contextvectors.bin';


%% Set paramters
p.encoding = 'ISO-8859-1';
p.vec2file = false;
p.collectInstanceStats = true;
p.context2phrase = false;
p.onlyGoldSingleInstances = false;
p.clusterTestData = false;
p.contextWidth = 10;
p.clustAlgo = 'kmeans';
p.postaggedvectors = false;
maxNumOfInstanses = 100;%1800; %:100:5000;


%% load data
mex -silent ../lib/readw2vfileMEX.c

tic;

if ~exist('wordvectorcont','var')
    wordvectorcont = readw2vfile(p.wordvectorpath,'mex',true,'debug',true, 'vec2file',p.vec2file,'encoding',p.encoding);
else
    wordvectorcont.reset();
end
 
if ~exist('contextvectorcont','var')
    contextvectorcont = readw2vfile(p.contextvectorpath,'mex',true,'debug',true, 'vec2file',p.vec2file,'encoding',p.encoding);
else
    contextvectorcont.reset();
end
p.vectorDim = length(wordvectorcont.get(''));

fprintf('Data loading time: %0.2fs\n',toc);

%% run experiment
% rng('shuffle');
rng(1);

map = wordvectorcont.map;
    
fprintf('Parameters for expSem1-> \n %s\n',evalc('disp(p)'));
%%
%senses = expSB(p, wordvectorcont, contextvectorcont);
       senseInducer = SenseInducer(p, p.textfilename, wordvectorcont, contextvectorcont...
                                ,'contextWidth',p.contextWidth...
                                ,'clustAlgo',p.clustAlgo...
                                ,'encoding',p.encoding);
fprintf('\n%s\n',senseInducer.tostring());

targetwords = {'rock','paper'};
for itarget = 1:length(targetwords)
    targetword=targetwords{itarget};
    
    fprintf('Inducing senses for %s...\n',targetword);
    [senses,~] = senseInducer.induceSenses(targetword);
    
    fprintf('%d senses found\n',length(senses.wordSenseIDs));
    
    
    % Save sense centroids
    cent = senses.wordSenseCentroids;
    save(['./bin/', senses.targetWord,'_sense_vectors.txt'],'cent','-ascii');
end  

classdef readw2vfile < handle
%readw2vfile Parses word2vec files into a dictionary 

    properties (SetAccess = private, GetAccess = private)
        status
        debug
        removenan
        zeroVector
        vec2file
        vec2fileID
    end
    properties (SetAccess = private)
        map
        vocab
        vectors
        vocabSize
        dim
        missingVectors
        vocabfile
    end
    methods
        function this = readw2vfile( filename ,varargin)
            
            this.reset();
            this.status = StatusWriter();
            
            %% parse parameters
            p = inputParser;   

            addRequired(p,'filename',@isstr);
            addOptional(p,'binary',true,@isnumeric);
            addOptional(p,'maxwordlen',200,@isnumeric);
            addOptional(p,'unicode',false,@islogical);
            addOptional(p,'removenan',false,@islogical);
            addOptional(p,'mex',false,@islogical);
            addOptional(p,'debug',false,@islogical);
            addOptional(p,'vocabfile','',@isstr);
            addOptional(p,'vec2file',false,@islogical);
            addOptional(p,'encoding','',@isstr);

            p.parse(filename, varargin{:});
            this.debug = p.Results.debug;
            this.removenan = p.Results.removenan;
            this.vocabfile = p.Results.vocabfile;
            this.vec2file = p.Results.vec2file;
            disp(p.Results);

            if p.Results.mex
                this.loadVectorsMEX(p.Results.filename, p.Results.encoding);
            else
                [this.vocab, this.vectors] = this.loadVectors(  p.Results.filename,...
                                                                p.Results.binary,...
                                                                p.Results.unicode);
            end
            this.zeroVector = zeros(this.dim,1);
                
            if this.vec2file
                f = fopen([filename '.matrix.bin'],'w');
                fwrite(f,this.vectors,'single');
                fclose(f);
                this.vec2fileID = fopen([filename '.matrix.bin'],'r');
                this.vectors = [];
            end
            this.map = containers.Map(this.vocab,1:this.vocabSize);
            
            this.status.lineFeed();
        end
        
        function loadVectorsMEX(this, filename, encoding)
            [vocabmat, this.vectors] = readw2vfileMEX(filename);
            
            this.vocabSize = size(vocabmat,2);
            this.dim = size(this.vectors,1);
            this.vocab = cell(1,this.vocabSize);
            for i=1:this.vocabSize
                w = vocabmat(1:find(vocabmat(:,i)==0,1)-1,i)';
                this.vocab{i} = native2unicode(w,encoding);
            end
            %error checking
            if ~this.removenan
                assert(~isnan( sum(sum(this.vectors)) ));
            end
     
        end
        function [words, vectors] = loadVectors(this,filename,binary,...
                unicode)
            this.status.printStatic('Loading %s, progress: ',filename);
            %% Read data
            if ~unicode
                fid = fopen(filename, 'r');
            else
                fid = fopen(filename, 'r','n','UTF-8');
            end
            
            separatevocab = true;
            if isempty(this.vocabfile)
                separatevocab = false;
            end

            this.vocabSize = fscanf(fid,'%d',1);
            this.dim = fscanf(fid,'%d',1);

            vectors = zeros(this.dim,this.vocabSize);
            words = cell(1,this.vocabSize);
            
            if separatevocab
                fscanf(fid,'\n',1);
                vectors = zeros(this.dim,0);
                if this.vocabSize>100
                    sizes = ceil(ones(1,9)*this.vocabSize/10);
                    sizes(end+1) = this.vocabSize-sum(sizes);
                else
                    sizes = [this.vocabSize];
                end
                for is = 1:length(sizes)
                    vis = fread(fid,[this.dim,sizes(is)],'*single');
                    vectors = [vectors vis];
                    
                    nDone = sum(sizes(1:is));
                    this.status.printProgress(nDone,this.vocabSize);
                end
                fprintf(1,'\nReading vocab...\n');
                words = readvocab(this.vocabfile, unicode);
            else
                for w=1:this.vocabSize

                    words{w} = strtrim(fscanf(fid,'%s',1));
                    fread(fid,1,'*char');

                    if binary
                        vectors(:,w) = fread(fid,this.dim,'*single');
                    else
                        vectors(:,w) = fscanf(fid,'%f',this.dim);
                    end
                    %% error checking
                    if isnan(sum(vectors(:,w)))
                        if ~this.removenan
                            error('vector for %s has nan\n',words{w}); 
                        else
                            fprintf(1,'vector for %s has nan\n',words{w});
                        end
                    end
                    if length(vectors(:,w)) ~= this.dim
                        error('vector for %s has incorrect dim %f\n',words{w},length(vectors(:,w))); 
                    end
                    %% status
                    if mod(w,50000) == 0 
                        this.status.printProgress(w,this.vocabSize);
                    end
                end
            end
            A = fread(fid);
            fclose(fid);            
        end
        
        function res = get(this,key)
            key = strtrim(key);  %is this really good?
            
            try
                index = this.map(key);
                if this.vec2file
                    singlebytes = 4;
                    fseek(this.vec2fileID,(singlebytes*this.dim)*(index-1),'bof');
                    res = fread(this.vec2fileID,[this.dim,1],'*single');
                else    
                    res = this.vectors(:,index);
                end
            catch
                res = this.zeroVector;
                
                if this.debug
                    try
                        this.missingVectors(key) = this.missingVectors(key)+1;
                    catch
                        this.missingVectors(key) = 1;
                        %this.status.printStatic('Vector missing for: "%s"\n',key); 
                    end
                end
            end
        end
        function reset(this)
            this.missingVectors = containers.Map();
        end
    end

end


/*==========================================================
 * readw2vfileMEX.c - c lib for reading word2vec files 
 *
 *
 * The calling syntax is:
 *
 *		[vectors vocab] = readw2vfileMEX(file_name)
 *
 * This is a MEX-file for MATLAB.
 *
 *========================================================*/

#include "mex.h"

const long long max_size = 2000;         /* max length of strings */
const long long max_w = 50;              /* max length of vocabulary entries */

/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
   
    double *inMatrix;               /* 1xN input matrix */
    double *outMatrix;              /* output matrix */
        char *filename;
    size_t buflen;
    int status;
    
    /* check for proper number of arguments */
    if(nrhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:readw2vfileMEX:nrhs","Two inputs required.");
    }
    if(nlhs!=2) {
        mexErrMsgIdAndTxt("MyToolbox:readw2vfileMEX:nlhs","Two output required.");
    }
    /* Check for proper input type */
    if (!mxIsChar(prhs[0]) || (mxGetM(prhs[0]) != 1 ) )  {
	    mexErrMsgIdAndTxt( "MATLAB:readw2vfileMEX:invalidInput", 
                "Input argument must be a string.");
    }
    
     /* Find out how long the input string is.  Allocate enough memory
       to hold the converted string.  NOTE: MATLAB stores characters
       as 2 byte Unicode ( 16 bit ASCII) on machines with multi-byte
       character sets.  You should use mxChar to ensure enough space
       is allocated to hold the string */
    
    buflen = mxGetN(prhs[0])*sizeof(mxChar)+1;
    filename = mxMalloc(buflen);
    
    /* Copy the string data into filename. */ 
    status = mxGetString(prhs[0], filename, (mwSize)buflen);   
    mexPrintf("Loading w2v vectors in:  %s\n", filename);
    
    /* Load word2vec file */
    FILE *f;
    char st1[max_size];
    char file_name[max_size], st[100][max_size];
    float dist, len, vec[max_size];
    long long words, size, a, b, c, d, cn, bi[100];
    char ch;
    float *M;
    char *vocab;
    
    f = fopen(filename, "rb");
    
    mxFree(filename);
    
    if (f == NULL) {
        mexErrMsgIdAndTxt("MyToolbox:readw2vfileMEX:nlhs","Input file not found.");
    }
    
    fscanf(f, "%lld", &words);
    fscanf(f, "%lld", &size);

    vocab = (char *)mxMalloc((long long)words * max_w * sizeof(char));
    M = (float *)mxMalloc((long long)words * (long long)size * sizeof(float));

    if (M == NULL) {
        mexErrMsgIdAndTxt("MyToolbox:readw2vfileMEX:M","Cannot allocate memory");
    }
    for (b = 0; b < words; b++) {
        a = 0;
        while (1) {
            vocab[b * max_w + a] = fgetc(f);
            if (feof(f) || (vocab[b * max_w + a] == ' ')) 
                break;
            if ((a < max_w) && (vocab[b * max_w + a] != '\n')) 
                a++;
        }
        vocab[b * max_w + a] = 0;
        for (a = 0; a < size; a++) 
            fread(&M[a + b * size], sizeof(float), 1, f);
        
    }
  
    fclose(f);
    
    plhs[0] = mxCreateNumericMatrix(0, 0, mxUINT8_CLASS, mxREAL);
	
    /* Put the C array into the mxArray and define its dimensions */
    mxSetPr(plhs[0], (double*)vocab);
    mxSetM(plhs[0], max_w);
    mxSetN(plhs[0], words);
    
    /*mxArray *arr = mxCreateCellMatrix( 1,words);
    mxArray *str;
    mwIndex i;
    for(i=0; i<words; i++) {
        str = mxCreateString(&vocab[i * max_w]);
        mxSetCell(arr, i, str);
    }
    plhs[0] = arr;
    mxFree(vocab);*/
    
    
    /* Create a 0-by-0 mxArray; you will allocate the memory dynamically */
    plhs[1] = mxCreateNumericMatrix(0, 0, mxSINGLE_CLASS, mxREAL);
	
    /* Put the C array into the mxArray and define its dimensions */
    mxSetPr(plhs[1], (double*)M);
    mxSetM(plhs[1], size);
    mxSetN(plhs[1], words);
    
    

    
}

